# soal-shift-sisop-modul-1-a04-2022 

|     NRP    |     Nama    |
| :--------- |:------------    |
| 5025201098 | Ibra Abdi Ibadihi
| 5025201184 | Cahyadi Surya Nugraha |
| 5025201007 | Sejati Bakti Raga

# soal shift sisop modul 1 A04 2022

# Soal 1
Output yang diinginkan dari soal 1 adalah untuk membuat sistem register dan login. Dimana setiap kegiatan login atau register akan disimpan pada sebuah log.txt sesuai dengan status dari kegiatan entah itu login. Detail yang disimpan dalam log sendiri adalah tanggal dan waktu kegiatan serta username yang melakukan kegiatan tersebut. Ketika user berhasil login, terdapat 2 command yang nantinya akan dijalankan, yaitu att dan dl. Pilihan att akan menampilkan berapa banyak percobaan yang telah dilakukan user untuk login (baik gagal maupun sukses). Sedangkan, Pilihan dl akan mendownload image dari web yang telah disediakan (https://loremflickr.com/320/240 )sebanyak N kali, kemudian image tersebut akan disimpan di dalam zip.

## Register 
Setiap register yang sukses akan disimpan pada /users/user.txt. 
User akan menginputkan username dan password. Terdapat beberapa persyaratan untuk pembuatan password, yaitu:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumerical
- Tidak boleh sama dengan username

Pada sistem register, yang paling pertama dilakukan adalah memastikan bahwa folder atau file dari output yang akan di redirect nanti ada, maka dari itu dibuatlah fungsi checkFileExist yang berfungsi untuk mengecek apakah folder atau folder output nanti sudah ada atau belum jika belum maka buatkan folder dan file tersebut menggunakan mkdir -p (-p ini untuk menghindari error dengan membuat parent direktorinya jika belum ada) dan menggunakan touch untuk membuat filenya.

Hal kedua yang harus dilakukan adalah mengecek apakah user yang diinput sudah terdaftar di user.txt dengan menggunakan grep -q -w (-w untuk mencocokan seluruh kata). Jika ditemukan maka program akan mengoutputkan "User already exist" dan melakukan pencatatan pada log.txt bahwa kegiatan tidak berhasil ($dates $time REGISTER:ERROR User already exist) dan program akan terhenti.

Jika tidak ditemukan maka sistem register akan melakukan validasi password dengan user akan diminta untuk memasukkan passwordnya sebanyak dua kali yang akan digunakan untuk memvalidasi dan memastikan kembali terhadap user bahwa user tersebut akan menggunakan password yang dituliskan pada field password sebelumnya. Pengecekan kesamaan password dan confirmpassword menggunakan operasi aritmatika !=, dimana jika hasilnya true atau artinya tidak sama maka akan mengoutputkan ke terminal "Password is not matches" dan melakukan exit. Jika bernilai false maka program akan lanjut ke tahap selanjutnya.

Tahap selanjutnya adalah mengecek syarat dari password yang telah diberikan yaitu:
- Mengecek minimal 8 karakter dengan mengecek apakah panjang karakter lebih sedikit dari (-lt) 8 atau tidak. Jika iya, maka proses dihentikan dan mengeluarkan message "Password length should at least be 8 characters", jika tidak lebih sedikit, maka lanjutkan untuk syarat selanjutnya.
- Mengecek minimal 1 huruf kapital pada password dengan menggunakan grep pada password. Jika tidak ditemukan maka akan mengeluarkan message "Password must contain at least one uppercase", jika memenuhi maka melanjutkan ke syarat berikutnya.
- Mengecek minimal 1 huruf kecil pada password dengan menggunakan grep pada password. Jika tidak ditemukan maka akan mengeluarkan message "Password must contain at least one lowercase", jika memenuhi maka melanjutkan ke SSproses pencatatan log.

Ketika semua persyaratan password dan username terpenuhi, maka print pesan log di log.txt dengan format MM/DD/YY HH:MM:SS  REGISTER:
INFO User **USERNAME** registered successfully. Dan disimpan pada user.txt dengan format MM/DD/YY HH:MM:SS **Username** **Password**

Proses Register selesai.

## Login

## Output

## Kendala

# Soal 2
Output yang diinginkan dari soal2 adalah untuk membuat suatu program yang dapat melakukan analisis melalui scripting awk. Dengan kasus pada soal berupa website (https://daffa.info) yang dihack oleh seseorang, user diminta untuk menganalisis rerata request per jam yang dikirimkan penyerang ke website, mengidentifikasi IP yang paling banyak melakukan request ke server, jumlah request dengan user-agent curl, serta daftar IP yang mengakses website pada jam 2 pagi. Nantinya output tersebut akan disimpan pada ratarata.txt dan result.txt.

## Melakukan Pengecekan Access
Langkah pertama yang harus dilakukan adalah pengecekan akses executable pada program shell scripting. Pada program ini otorisasi akses hanya diberikan kepada superuser atau root untuk menjalankan program agar lebih optimal.

## Memvalidasi Log File
Sebuah file log akan dibutuhkan untuk mencatat proses selama program berjalan di sesi tertentu. Jika file dalam direktori yang diberikan (dalam kasus ini: ~/logfile_daffainfo.log) belum tersedia, maka file log harus dibuat terlebih dahulu. Sebaliknya, jika file sudah tersedia, maka sesi log tersebut akan dihapus dan diganti dengan sesi baru, sehingga proses yang dilakukan adalah menghapus file log sebelumnya dan membuat file log yang baru.

## Memvalidasi Folder
Dilakukan validasi folder yang memiliki tujuan yang sama dengan validasi log file. Metode yang digunakan adalah dengan shell script: -d "<lokasi folder>" untuk mendeteksi ada atau tidaknya file pada lokasi tersebut. 

## Rata-Rata Serangan (x Request per Jam)
Setelah melakukan validasi folder dan file, hal yang selanjutnya dilakukan adalah mengoutputkan hal-hal yang telah diminta pada soal. Pada kasus ini, hal pertama yang diminta dioutputkan dari soal adalah rata-rata serangan yang akan disimpan pada file ratarata.txt dengan format penulisan yang telah dideskripsikan pada soal.
Untuk melakukan penghitungan rerata serangan, digunakanlah AWK looping melalui array yang telah diolah melalui gsub (global substitution) untuk mensubstitusikan input IP yang ada menjadi format IP yang diinginkan. Setelah itu dilakukan, program akan melakukan loop dalam array yang telah diolah dengan variabel "i" yang menotasikan setiap nilai dalam array tersebut. Dalam loop tersebut, akan dilakukan increment pada variabel hour dan addition pada nilai rerata (avg) sebanyak nilai array pada index ke-i. Setelah loop tersebut selesai, baru dilakukan averaging dengan membagi variabel avg dengan jumlah jam (hour) yang telah diproses dari loop. Kemudian hasil berupa nilai dari avg akan diprint dan dimasukkan ke dalam file ratarata.txt.

## IP yang Paling Banyak Mengakses Server
Lalu output berikutnya adalah mencari tahu IP yang paling banyak mengakses server yang akan disimpan pada file result.txt dengan format penulisan yang telah dideskripsikan pada soal.
Untuk mengetahui IP yang paling banyak mengakses server, dilakukan langkah yang mirip seperti sebelumnya hanya saja kondisi pada gsub sedikit dirubah disesuaikan dengan kasus yang diinginkan. Kemudian dicari IP dan jumlah request terbanyak yang ada dalam array, variabel "address" merupakan IP yang paling banyak mengakses server dan variabel "max" adalah jumlah request terbanyak.

## Requests yang menggunakan user-agent curl
Untuk output ke-3 sama seperti sebelumnya akan dioutputkan ke dalam file result.txt. Untuk menghitung banyak request yang menggunakan curl sebagai user-agent, digunakan AWK dimana setiap ditemukan curl sebagai user agent maka variabel n terus bertambah. Hasil n merupakan banyaknya request yang menggunakan curl sebagai user agent akan diprint dan dimasukkan ke dalam file result.txt

## IP yang mengakses website pada jam 2 pagi
Output terakhir juga akan disimpan ke dalam file result.txt. Untuk mencari IP yang mengakses website pada jam 2 pagi, digunakan AWK dimana setiap ditemukan 2022:02 maka arr[i] terus bertambah. Kemudian menggunakan looping akan di print IP yang mengakses pada jam 2 pagi.
