#!/bin/bash

filename=$(date "+%Y%m%d%H%M%S")

free -m >> metrics_${filename}.log
du -sh /home/user >> metrics_${filename}.log
